# java.time.format.DateTimeFormatter desugaring issue

Minimal Complete Verifiable Example of an issue with formatting months with `java.time.format.DateTimeFormatter` with AGP library desugaring turned on.

Run the `desugar` app to see the issue. The displayed text formats March 2023 as "3 2023" with pattern specified as `LLLL y`.

Run the `no-desugar` app to see the expected result. `no-desugar` is a copy of `desguar` with only a single change: `coreLibraryDesugaringEnabled true` is commented out in `build.gradle`.
